# Phalcon7 Devtools

## What are Devtools?

This tools provide you useful scripts to generate code helping to develop faster and easy applications that use
with Phalcon framework.

## Requirements

* PHP >= 7.4
* Phalcon7 >= 1.0.0

## Usage

To get a list of available commands just execute following:

```bash
php scaffold.php --style=boostrap --table=robots
```

## Database adapter

Should add `adapter` parameter in your `db` config file (if you use not Mysql database).

For PostgreSql it will be something like:

```php
$config = [
  'host'     => 'localhost',
  'dbname'   => 'my_db_name',
  'username' => 'my_db_user',
  'password' => 'my_db_user_password',
  'adapter'  => 'Postgresql'
];
```
