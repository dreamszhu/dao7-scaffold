<?php

abstract class BaseBuildTask extends \Phalcon\Cli\Task
{
	public static function info($message)
	{
		echo Phalcon\Cli\Color::info($message);
	}

	public static function success($message)
	{
		echo Phalcon\Cli\Color::success($message);
	}

	public static function error($message)
	{
		echo Phalcon\Cli\Color::error($message);
	}

	public static function GenerateFile($src, $dest, $replace = NULL, $overwrite = FALSE)
	{
		self::info('Generate src '.$src);
		self::info('Generate file '.$dest);
		if (!file_exists(dirname($dest))) {
			mkdir(dirname($dest), 0777, true);
		}

		if ($overwrite || !file_exists($dest)) {
			$code = file_get_contents($src);
			if (is_array($replace)) {
				$code = strtr($code, $replace);
			}
			file_put_contents($dest, $code);
		}
	}

}
