<?php
namespace Scaffold;

class BuildTask extends \BaseBuildTask
{
	// php scaffold.php
	public function runAction(\Options $options)
	{
		var_dump($options);
		switch ($options->type) {
			case 'project':
				self::GeneratorProject($options);
				break;
			case 'controller':
				self::GeneratorController($options);
				break;
			case 'model':
				self::GeneratorModel($options);
				break;
			case 'view':
				self::GeneratorView($options);
				break;
			default:
				echo self::error('type error');
				break;
		}
	}

	public static function GeneratorProject(\Options $options) {
		$templateFile = $options->get('templatedir') . '/scaffold/public/index.php';
		$outpath = rtrim($options->get('outdir'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
        $outFile = $outpath.'public'.DIRECTORY_SEPARATOR.'index.php';
		self::GenerateFile($templateFile, $outFile, NULL, $options->get('overwrite'));

		self::GeneratorAssets($options);
		self::GeneratorController($options);
		self::GeneratorModel($options);
		self::GeneratorView($options);
	}

	public static function GeneratorAssets(\Options $options) {
		$outpath = rtrim($options->get('outdir'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;

		$templateFile = $options->get('templatedir') . '/scaffold/public/css/bootstrap.min.css';
        $outFile = $outpath.'public'.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.'bootstrap.min.css';
		self::GenerateFile($templateFile, $outFile, NULL, $options->get('overwrite'));

		$templateFile = $options->get('templatedir') . '/scaffold/public/js/bootstrap.min.js';

        $outFile = $outpath.'public'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'bootstrap.min.js';
		self::GenerateFile($templateFile, $outFile, NULL, $options->get('overwrite'));

		$templateFile = $options->get('templatedir') . '/scaffold/public/js/jquery.min.js';
        $outFile = $outpath.'public'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'jquery.min.js';
		self::GenerateFile($templateFile, $outFile, NULL, $options->get('overwrite'));
	}

	public static function GeneratorController(\Options $options) {
		$fullModelName = $options->getClassName('model');
		if (empty($fullModelName)) {
			echo self::error('GeneratorController, model name error');
			return;
		}

		$fullControllerName = $options->getClassName('controller');
		if (empty($fullControllerName)) {
			$fullControllerName = \Phalcon\Kernel::getClassName($fullModelName);
		}
		if (empty($fullControllerName)) {
			echo self::error('GeneratorController, controller name error');
			return;
		}
		$fullControllerName = \Phalcon\Text::camelize($fullControllerName);
		$controllerName = \Phalcon\Kernel::getClassName($fullControllerName);
		$namespace = \Phalcon\Kernel::getNamespace($fullControllerName);

		$outpath = rtrim($options->get('outdir'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
		$path = trim(str_replace('\\', DIRECTORY_SEPARATOR, $namespace), DIRECTORY_SEPARATOR);
		if ($path) {
			$fullpath = $outpath.'app'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR;
		} else {
			$fullpath = $outpath.'app'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR;
		}

		$templateFile = $options->get('templatedir') . '/scaffold/Controller.php';
        $outFile = $fullpath.$controllerName.'Controller.php';

		self::GenerateFile($templateFile, $outFile, [
			'$uri$' => $options->get('uri') ?: '/'.$controllerName,
			'$viewpath$' => $path ? $path.DIRECTORY_SEPARATOR.strtolower($controllerName).DIRECTORY_SEPARATOR : strtolower($controllerName).DIRECTORY_SEPARATOR,
			'$defineNamespace$' => $namespace ? 'namespace '.$namespace.';' : '',
			'$controllerName$' => $controllerName,
			'$fullControllerName$' => $fullControllerName,
			'$rootFullModelName$' => '\\'.$fullModelName,
			'$fullModelName$' => $fullModelName,
			'$pk$' => $options->get('pk'),
			'$pkVar$' => '$'.$options->get('pk'),
		], $options->get('overwrite'));
	}

	public static function GeneratorModel(\Options $options) {
		$fullModelName = $options->getClassName('model');
		if (empty($fullModelName)) {
			echo self::error('GeneratorModel fail, model name error');
			return;
		}
		$fullModelName = \Phalcon\Text::camelize($fullModelName);
		$modelName = \Phalcon\Kernel::getClassName($fullModelName);
		$namespace = \Phalcon\Kernel::getNamespace($fullModelName);

		$outpath = rtrim($options->get('outdir'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
		$path = trim(str_replace('\\', DIRECTORY_SEPARATOR, $namespace), DIRECTORY_SEPARATOR);
		if ($path) {
			$fullpath = $outpath.'app'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR;;
		} else {
			$fullpath = $outpath.'app'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR;
		}

		$templateFile = $options->get('templatedir').DIRECTORY_SEPARATOR.'scaffold'.DIRECTORY_SEPARATOR.'Model.php';
        $outFile = $fullpath.$modelName.'.php';

		self::GenerateFile($templateFile, $outFile, [
			'$defineNamespace$' => $namespace ? 'namespace '.$namespace.';' : '',
			'$modelName$' => $modelName,
			'$tableName$' => $options->get('table') ?: strtolower($modelName),
		], $options->get('overwrite'));
	}

	public static function GeneratorView($options) {
		$fullModelName = $options->getClassName('model');
		if (empty($fullModelName)) {
			echo self::error('GeneratorView faild, model name error');
			return;
		}
		$modelName = \Phalcon\Kernel::getClassName($fullModelName);

		$fullControllerName = $options->getClassName('controller');
		if (empty($fullControllerName)) {
			$fullControllerName = \Phalcon\Kernel::getClassName($fullModelName);
		}
		if (empty($fullControllerName)) {
			echo self::error('GeneratorView faild, controller name error');
			return;
		}
		$fullControllerName = strtolower($fullControllerName);
		$controllerName = \Phalcon\Kernel::getClassName($fullControllerName);
		$namespace = \Phalcon\Kernel::getNamespace($fullControllerName);

		$outpath = rtrim($options->get('outdir'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
		$path = trim(str_replace('\\', DIRECTORY_SEPARATOR, $fullControllerName), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
		
		$viewrootpath = $outpath.'app'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;
		$fullpath = $viewrootpath.$path.DIRECTORY_SEPARATOR;;

		$templatePath = $options->get('templatedir').DIRECTORY_SEPARATOR.'scaffold'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.$options->get('style').DIRECTORY_SEPARATOR;

		$viewFile = $templatePath.'form.phtml';
        $outFile = $fullpath.'form.phtml';

		self::GenerateFile($viewFile, $outFile, [
			'$pk$' => $options->get('pk'),
		], $options->get('overwrite'));

		$viewFile = $templatePath.'index.phtml';
        $outFile = $fullpath.'index.phtml';

		self::GenerateFile($viewFile, $outFile, [
			'$modelName$' => $modelName,
			'$uri$' => $options->get('uri') ?: '/'.$controllerName,
			'$pk$' => $options->get('pk'),
		], $options->get('overwrite'));

		$viewFile = $templatePath.'paginator.phtml';
        $outFile = $viewrootpath.'shared'.DIRECTORY_SEPARATOR.'paginator.phtml';

		self::GenerateFile($viewFile, $outFile, NULL, $options->get('overwrite'));

		$viewFile = $templatePath.'layout.phtml';
        $outFile = $viewrootpath.'index.phtml';

		self::GenerateFile($viewFile, $outFile, NULL, $options->get('overwrite'));
	}
}
