<?php

error_reporting(E_ALL);
mb_internal_encoding("UTF-8");
if (isset($_SERVER["REQUEST_URI"])) {
	$parts = parse_url($_SERVER["REQUEST_URI"]);
	$uri = $parts["path"];
} else {
	$uri = NULL;
}
unset($_GET['_url']);

try {
	$config_basedir = __DIR__ . '/../apps/config/';
	Phalcon\Config\Adapter\Php::setBasePath($config_basedir);

	// $config = new Phalcon\Config\Adapter\Php('config.php');

	if (isset($_GET['debug'])) {
		Phalcon\Debug::enable();
	} else {
		Phalcon\Debug::disable();
	}

	$loader = new \Phalcon\Loader();

	// 类的加载路径，按照先后顺序查找
	$loader->registerDirs(
			array(
				__DIR__ . '/../app/controllers/',
				__DIR__ . '/../app/models/',
			)
	)->register();

	$di = new \Phalcon\DI\FactoryDefault();

	$di->set('url', function () use ($config) {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	}, TRUE);

	$di->set('view', function () {
		$view = new \Phalcon\Mvc\View();
		// 视图根路径可以有多个，按照先后顺序查找
		$view->setBasePath([
			__DIR__ . '/../app/views/',
		]);
		return $view;
	}, TRUE);

	/**
	$di->set('url', function () use ($config) {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri($config->baseUri);
		return $url;
	}, TRUE);
	$di->set('config', function () use ($config) {
		$config['docroot'] = __DIR__ . DIRECTORY_SEPARATOR;
		return $config;
	}, TRUE);
	$di->setShared('redis', function() use ($config) {
		$redis = new \Redis();
		$redis->connect($config->redis->host, $config->redis->port);
		if ($config->redis->db) {
			$redis->select($config->redis->db);
		}
		$redis->auth($config->redis->password);
		return $redis;
	});

	$di->setShared('modelsCache', function() use ($config) {
		$frontCache = new Phalcon\Cache\Frontend\Data(array('lifetime' => 3600));
		$cache = new Phalcon\Cache\Backend\Redis($frontCache, array(
			'redis' => 'redis',
			'statsKey' => false
		));
		return $cache;
	});

	$di->set('modelsMetadata', function() use ($config) {
		$metaData = new \Phalcon\Mvc\Model\Metadata\Files(array(
			'metaDataDir' => $config->cacheDir. 'metadata/'
		 ));
		 return $metaData;
	}, TRUE);
	*/

	$di->set('db', function () {
		return new \Phalcon\Db\Adapter\Pdo\Sqlite(['dbname' => '/tmp/phalcon_test.sqlite']);
	}, TRUE);

	if (\Phalcon\Debug::isEnable()) {
		// 调试模式预先开启 session
		$session = new Phalcon\Session\Adapter\Files;
		$session->start();

		$di->set('session', function () use ($session) {
			return $session;
		}, TRUE);
	} else {
		$di->set('session', function () {
			$session = new Phalcon\Session\Adapter\Files;
			$session->start();
			return $session;
		}, TRUE);
	}

	$di->set('flash', function() {
		$flash = new \Phalcon\Flash\Session;
		return $flash;
	});

	$di->set('crypt', function() {
		$crypt = new Phalcon\Crypt();
		$crypt->setKey('1234567891111212');
		return $crypt;
	});

	$di['router'] = function() {
		$router = new \Phalcon\Mvc\Router();
		//$router->setUriSource(\Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI);

		return $router;
	};

	$application = new \Phalcon\Mvc\Application($di);
	$application->handle($uri)->send();
} catch (\Exception | \Error $e) {
	printf($e->getMessage());
}
Phalcon\Debug::disable();
