<?php

$opts = new \Phalcon\Cli\Options('Phalcon7 DevTools (1.3.4)');

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_BOOLEAN,
    'allowEmpty' => false,
    'name' => 'overwrite',
	'help' => "是否覆盖",
    'required' => false,
    'defaultValue' => true
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_BOOLEAN,
    'allowEmpty' => false,
    'name' => 'camelize',
	'help' => "对 controller、model 名是否自动转为驼峰格式",
    'required' => true,
    'defaultValue' => true
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'style',
	'help' => "指定视图使用的样式",
    'required' => false,
    'defaultValue' => 'boostrap'
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'controller',
	'help' => '对 controller 命名，默认使用 model 相同命名，例如：admin\user',
    'required' => false
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'model',
	'help' => "对 model 命名",
    'required' => true
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'table',
	'help' => "指定表名，如果未指定则使用 model 名",
    'required' => false
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'pk',
	'help' => '指定主键名',
    'required' => true,
    'defaultValue' => 'id'
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'type',
	'help' => "指定创建类型 project controller、model、view",
    'required' => false,
    'defaultValue' => 'project'
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'templatedir',
	'help' => "模板目录",
    'required' => false,
    'defaultValue' => __DIR__.DIRECTORY_SEPARATOR.'templates'
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'outdir',
	'help' => "输出目录",
    'required' => false,
    'defaultValue' => __DIR__.DIRECTORY_SEPARATOR.'out'
]);

$opts->add([
    'type' => \Phalcon\Cli\Options::TYPE_STRING,
    'allowEmpty' => false,
    'name' => 'uri',
	'help' => "指定uri，如果未指定则使用 controller 名",
    'required' => false
]);

$vals = $opts->parse();
if (!$vals) {
	return;
}

// 使用CLI工厂类作为默认的服务容器
$di = new Phalcon\Di\FactoryDefault\Cli();

// 定义应用目录路径
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__)));

/**
 * 注册类自动加载器
 */
$loader = new \Phalcon\Loader();
$loader->registerDirs(
    array(
        APPLICATION_PATH . DIRECTORY_SEPARATOR . 'scripts',
    )
);
$loader->register();

// 创建console应用
$console = new Phalcon\Cli\Console();

// 处理console应用参数
$arguments = array(
	'namespace' => 'scaffold',
	'task' => 'build',
	'action' => 'run',
	'params' => [new \Options($vals)]
);
$console->handle($arguments);
